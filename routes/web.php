<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'ArticuloController@getIndex')->name('index');
Route::get('admin', 'ArticuloController@getAdmin')->name('area_admin');
Route::get('/add', 'ArticuloController@crearArticulo')->name('crear_nuevo_articulo');
Route::get('/login', 'UserController@postLogin')->name('login');
Route::get('logout', 'UserController@getLogout')->name('logout');