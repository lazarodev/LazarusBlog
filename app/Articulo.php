<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $table = 'articulos';

    protected $fillabel = [
        'titulo', 
        'contenido', 
        'idAutor'
    ];

    public $timestamps = true;

    public function Autor() {
        return $this->belongsTo('App\User', 'idAutor');
    }
}
