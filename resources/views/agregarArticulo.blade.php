<html>

<head>

    <title>Mi primer Blog</title>
    <link rel="stylesheet" type"text/css" href="/assets/css/style.css">

</head>

<body>
    @if(Auth::check())
    <section>
        <div>
            <h1>Bievenidos al area de Administracion, { {Auth::user()->name} } </h1>
            <form name="crearArticulo" method="POST" action="{{ URL::route('crear_nuevo_articulo') }}">
            {{ csrf_field() }}
            <p>
                <input type="text" name="titulo" placeholder="Titulo del articulo" value="" />
            </p>
            <p>
                <textarea name="contenido" placeholder"Contenido del articulo"></textarea>
            </p>
            <p>
                <input type="submit" name"submit"/>
            </p>
        </form>
    </div>
    </section>
    @else
    <section>
        <div class="login">
            <h1>Login</h1>
            <form name="login" method="POST" action="{{ URL::route('login') }}">
                {{ csrf_field() }}
                <p>
                    <input type="text" name"email" value"" placeholder="Email">
                </p>
                <p>
                    <input type="password" name="password" value="" placeholder="Password">
                </p>
                <p class="submit">
                    <input type="submit" name"commit" value="Login">
                </p>
            </form>
        </div>
    </section>
    @endif
</body>

</html>